def is_triangle(a, b, c):
    cond_a = a < b + c
    cond_b = b < a + c
    cond_c = c < a + b
    return all((cond_a, cond_b, cond_c))